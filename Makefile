PREFIX=/usr/local
PREFIX_MAN=$(PREFIX)/man
CFLAGS?=-g -Wall

all: update-gandi-ipv6

update-gandi-ipv6: update-gandi-ipv6.o
	$(CC) $(CFLAGS) -L/usr/local/lib *.o -lcurl -pthread $(LDFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -I/usr/local/include -c $<

clean:
	rm -rf *.o *.core update-gandi-ipv6 makefile.depend

dep:
	$(CC) -MM *.c > makefile.depend

install:
	mkdir -p -m 755 $(PREFIX)/bin
	install -m 755 update-gandi-ipv6 $(PREFIX)/bin/update-gandi-ipv6

update-gandi-ipv6.o: update-gandi-ipv6.c xs.h xs_time.h xs_unicode.h \
 xs_json.h xs_io.h xs_curl.h
