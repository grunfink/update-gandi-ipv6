# update-gandi-ipv6

This is a tool that uses the GANDI.net LiveDNS API to update AAAA records whenever the IPV6 addresses of a host change.

*NOTE: I no longer use GANDI.net as my DNS registrar, so this software is unmaintained.*

## Prerrequisites

- A domain registered at GANDI.net (obviously)
- A GANDI.net API key (provided by them)

## Compilation and installation

`update-gandi-ipv6` is written in C and its only dependency is `libcurl`.

On Debian/Ubuntu, you can satisfy this requirement by running

```
    apt install libcurl4-openssl-dev
```

On OpenBSD:

```
    pkg_add curl
```

Run `make` and then `make install` as root.

## Usage

`update-gandi-ipv6` usually takes no arguments; the optional `-n` argument is accepted for running in *dry-run* mode, i.e. to perform all actions except the real update of the DNS zone. It's meant to be run from a `cron` job or similar. It only generates output when the IPV6 addresses of the machine it runs on are different from those stored in the DNS zone.

The first time you execute it from a terminal, it will prompt for three mandatory values: the GANDI API key, the domain name and the host name (providing a guess by default). This creates the configuration file, `~/.config/update-gandi-ipv6.json`, which contains some more knobs to tune the program's behaviour. On subsequent executions, `update-gandi-ipv6` will run according this configuration and never run in interactive mode again.

Loopback interfaces are ignored. Local address are also ignored by default, but this can be changed (see below).

## Configuration options

The configuration file is in JSON format. By default, it's a JSON object and contains the following configurable values:

- `api_key`: The GANDI api key you provided the first time.
- `domain`: The domain name you provided the first time.
- `host_name`: The host name you provided the first time.
- `only_for_ifaces`: A JSON list of strings containing the interface names that will be scanned. Interfaces not in this list will be skipped. If it's empty, all interfaces are scanned.
- `default_ttl`: The TTL (Time To Live) in seconds for the AAAA records that are created.
- `dbglevel`: The debug level. The default level (0) is silent unless the DNS zone is updated. Higher values give more information. -1 makes it completely mute. You can override this value by setting the DEBUG environment variable.
- `address_set`: Selects the set of addresses that will be used to update the zone. It's a string that contains one of the next three values: `"non_local"` (the default), to change only non-local address (i.e. those not starting with `fe80:`); `"local_only"`, the inverse of the previous one (i.e. use only local address), or `"any"`, to use all of them.

DNS allows for more than one address to be set for the same host name and `update-gandi-ipv6` respects it.

Example configuration:

```
{
    "address_set" : "non_local",
    "api_key" : "aaaBBBcccwhatever",
    "dbglevel" : 0,
    "default_ttl" : 300,
    "domain" : "example.com",
    "host_name" : "devmachine1",
    "only_for_ifaces" : [
        "eth0"
    ]
}
```

Since version 1.03, the configuration file can contain an array of these objects, to update as many host names and domains and api keys as you wish.

Example configuration with multiple setups (1.03 onward):

```
[
    {
        "address_set" : "non_local",
        "api_key" : "SomeAsCiIGarBage",
        "dbglevel" : 0,
        "default_ttl" : 300,
        "domain" : "example.org",
        "host_name" : "homer",
        "only_for_ifaces" : [
            "eth0"
        ]
    },
    {
        "address_set" : "local_only",
        "api_key" : "blahBlahBalh",
        "dbglevel" : 0,
        "default_ttl" : 300,
        "domain" : "example.com",
        "host_name" : "mrincognito",
        "only_for_ifaces" : [
            "wlan0"
        ]
    }
]
```

# License

See the LICENSE file for details.

# Author

grunfink @grunfink@comam.es
