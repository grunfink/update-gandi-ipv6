/* copyright (c) 2023 grunfink - MIT license */

#define VERSION "1.03-dev"

#define XS_IMPLEMENTATION

#include "xs.h"
#include "xs_time.h"
#include "xs_unicode.h"
#include "xs_json.h"
#include "xs_io.h"
#include "xs_curl.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <pwd.h>

int dbglevel = 0;

int dry_run = 0;


void ugi6_debug(int level, xs_str *str)
/* logs a debug message */
{
    if (dbglevel >= level) {
        xs *tm = xs_str_localtime(0, "%H:%M:%S");
        fprintf(stderr, "%s %s\n", tm, str);
    }

    xs_free(str);
}


xs_str *get_config_file_name(void)
/* gets the configuration file name */
{
    xs_str *fn = NULL;
    struct passwd *p;
    char *h;

    /* get the user id */
    if ((p = getpwuid(getuid())) != NULL)
        fn = xs_dup(p->pw_dir);
    else
    if ((h = getenv("HOME")) != NULL)
        fn = xs_dup(h);

    if (fn == NULL)
        ugi6_debug(0, xs_fmt("Error getting user directory -- cannot continue"));
    else
        /* add the directory and file name */
        fn = xs_str_cat(fn, "/.config/update-gandi-ipv6.json");

    return fn;
}


xs_dict *save_config(const char *fn, xs_dict *config)
/* saves the configuration */
{
    FILE *f;

    if ((f = fopen(fn, "w")) != NULL) {
        xs *s = xs_json_dumps(config, 4);

        fwrite(s, strlen(s), 1, f);
        fclose(f);
    }
    else
        ugi6_debug(0, xs_fmt("Error writing %s", fn));

    return config;
}

const char *default_config = "{"
    "\"api_key\": \"\","
    "\"domain\": \"\","
    "\"host_name\": \"\","
    "\"only_for_ifaces\": [],"
    "\"address_set\": \"non_local\","
    "\"dbglevel\": 0,"
    "\"default_ttl\": 300"
"}";

xs_dict *create_config(const char *fn)
/* prompts for a new information */
{
    printf("Gandi API Key:\n");
    xs *api_key = xs_strip_i(xs_readline(stdin));
    if (xs_is_null(api_key) || api_key[0] == 0) {
        printf("Cancelled\n");
        return NULL;
    }

    printf("Domain:\n");
    xs *domain = xs_strip_i(xs_readline(stdin));
    if (xs_is_null(domain) || domain[0] == 0) {
        printf("Cancelled\n");
        return NULL;
    }

    char def_hostname[1024];
    gethostname(def_hostname, sizeof(def_hostname));
    char *p = strchr(def_hostname, '.');
    if (p)
        *p = '\0';


    printf("Host name [%s]:\n", def_hostname);
    xs *host_name = xs_strip_i(xs_readline(stdin));
    if (xs_is_null(host_name) || host_name[0] == 0) {
        xs_free(host_name);
        host_name = xs_dup(def_hostname);
    }

    xs_dict *c = xs_json_loads(default_config);

    c = xs_dict_set(c, "api_key",   api_key);
    c = xs_dict_set(c, "domain",    domain);
    c = xs_dict_set(c, "host_name", host_name);

    return save_config(fn, c);
}


xs_list *get_ipv6(xs_dict *config)
/* iterates the ifaces and picks the most appropriate */
{
    struct ifaddrs *ifa, *t;
    xs_list *addrs = xs_list_new();
    xs_list *only_for_ifaces = xs_dict_get(config, "only_for_ifaces");
    xs_str *address_set = xs_dict_get(config, "address_set");
    int use_local = 0;
    int use_public = 1;

    /* address_set options:
       "non_local" (default): only public addresses
       "local_only": only local addresses
       "any": all kinds of addresses */
    if (strcmp(address_set, "local_only") == 0) {
        use_local = 1;
        use_public = 0;
    }
    else
    if (strcmp(address_set, "any") == 0)
        use_local = 1;

    getifaddrs(&ifa);

    for (t = ifa; t; t = t->ifa_next) {
        struct sockaddr *addr = t->ifa_addr;

        /* exclude undesired interfaces */
        if (xs_list_len(only_for_ifaces) && xs_list_in(only_for_ifaces, t->ifa_name) == -1) {
            ugi6_debug(1, xs_fmt("excluding interface %s", t->ifa_name));
            continue;
        }

        /* exclude interfaces not up */
        if (!(t->ifa_flags & IFF_UP))
            continue;

        /* exclude loopbacks */
        if (t->ifa_flags & IFF_LOOPBACK)
            continue;

        if (addr && addr->sa_family == AF_INET6) {
            char tmp[1024];

            struct sockaddr_in6 *in6 = (struct sockaddr_in6 *)addr;

            inet_ntop(AF_INET6, &in6->sin6_addr, tmp, sizeof(tmp));

            /* is it a local address? */
            if (xs_startswith(tmp, "fe80:")) {
                if (!use_local) {
                    ugi6_debug(1, xs_fmt("excluding local address %s", tmp));
                    continue;
                }
            }
            else
                if (!use_public) {
                    ugi6_debug(1, xs_fmt("excluding public address %s", tmp));
                    continue;
                }

            xs *s = xs_str_new(tmp);
            addrs = xs_list_append(addrs, s);
        }
    }

    freeifaddrs(ifa);

    return addrs;
}


xs_dict *open_config(void)
/* opens the configuration file */
{
    xs *fn = NULL;
    xs_dict *config = NULL;
    FILE *f;

    if ((fn = get_config_file_name()) == NULL)
        return NULL;

    if ((f = fopen(fn, "r")) != NULL) {
        xs *j = xs_readall(f);
        fclose(f);

        if ((config = xs_json_loads(j)) == NULL)
            ugi6_debug(0, xs_fmt("Error parsing %s -- cannot continue", fn));
    }
    else
    if (isatty(fileno(stdin)))
        config = create_config(fn);
    else
        ugi6_debug(0, xs_fmt("Cannot open nor create %s -- cannot continue", fn));

    dbglevel = xs_number_get(xs_dict_get(config, "dbglevel"));

    char *v;
    if ((v = getenv("DEBUG")) != NULL) {
        dbglevel = atoi(v);
        ugi6_debug(0, xs_fmt("DEBUG level set to %d from environment", dbglevel));
    }

    return config;
}


int dialog(xs_dict *config)
/* communicates with the API, optionally updating the records */
{
    xs *headers = xs_dict_new();
    int status  = 0;
    xs_list *p;
    xs_val *v;
    int needs_update = 0;
    int found = 0;

    if (dry_run)
        ugi6_debug(1, xs_fmt("running in dry run mode"));

    char *host_name = xs_dict_get(config, "host_name");
    char *domain    = xs_dict_get(config, "domain");

    xs *api_url = xs_fmt("https:/" "/api.gandi.net/v5/livedns/domains/%s/records", domain);

    /* get the current addresses */
    xs *addresses = get_ipv6(config);

    if (xs_list_len(addresses) == 0) {
        ugi6_debug(0, xs_fmt("cannot get any ipv6 address for this host -- cannot continue"));
        return 0;
    }

    xs *addr_str = xs_join(addresses, ",");
    ugi6_debug(1, xs_fmt("Addresses for %s: %s", host_name, addr_str));

    /* create the new record */
    xs *new_record = xs_dict_new();
    new_record = xs_dict_append(new_record, "rrset_name",   host_name);
    new_record = xs_dict_append(new_record, "rrset_type",   "AAAA");
    new_record = xs_dict_append(new_record, "rrset_values", addresses);
    new_record = xs_dict_append(new_record, "rrset_ttl",    xs_dict_get(config, "default_ttl"));

    /* create the headers */
    xs *s = xs_fmt("Apikey %s", xs_dict_get(config, "api_key"));

    headers = xs_dict_append(headers, "authorization", s);
    headers = xs_dict_append(headers, "content-type",  "application/json");

    /* first request: get the full zone */
    xs *p1 = NULL;
    int ps1 = 0;

    xs *r1 = xs_http_request("GET", api_url, headers, NULL, 0, &status, &p1, &ps1, 10);

    if (status != 200) {
        ugi6_debug(0, xs_fmt("API error getting records %d", status));
        return 0;
    }

    xs *records = xs_json_loads(p1);
    if (records == NULL) {
        ugi6_debug(0, xs_fmt("Error reading records from API"));
        return 0;
    }

    /* create a new zone with the record replaced, if needed */
    xs *new_records = xs_list_new();

    p = records;
    while (xs_list_iter(&p, &v)) {
        char *r_rec_type  = xs_dict_get(v, "rrset_type");
        char *r_host_name = xs_dict_get(v, "rrset_name");
        char *r_addresses = xs_dict_get(v, "rrset_values");

        if (strcmp(r_rec_type, "AAAA") == 0 && strcmp(r_host_name, host_name) == 0) {
            /* record found: compare addresses */
            xs *r_addr_str = xs_join(r_addresses, ",");

            ugi6_debug(1, xs_fmt("Current AAAA record for host %s: %s", host_name, r_addr_str));

            if (strcmp(addr_str, r_addr_str) == 0) {
                /* same addresses: nothing to change */
                new_records = xs_list_append(new_records, v);
                ugi6_debug(1, xs_fmt("DNS record update not needed for %s", host_name));
            }
            else {
                /* add the new record */
                new_records = xs_list_append(new_records, new_record);
                needs_update = 1;
            }

            found = 1;
        }
        else
            new_records = xs_list_append(new_records, v);
    }

    if (!found) {
        new_records = xs_list_append(new_records, new_record);
        needs_update = 1;
    }

    /* no update needed? done */
    if (!needs_update)
        return 1;

    ugi6_debug(1, xs_fmt("DNS zone update needed"));

    /* build the body object */
    xs *o = xs_dict_new();
    o = xs_dict_append(o, "items", new_records);

    /* second request: update zone */
    xs *b2  = xs_json_dumps(o, 4);
    int bs2 = strlen(b2);
    xs *p2  = NULL;
    int ps2 = 0;

    ugi6_debug(2, xs_fmt("PUT body: %s", b2));

    if (!dry_run) {
        xs *r2 = xs_http_request("PUT", api_url, headers, b2, bs2, &status, &p2, &ps2, 10);

        if (p2)
            ugi6_debug(2, xs_fmt("Reply: %s", p2));

        if (status < 200 || status > 299) {
            ugi6_debug(0, xs_fmt("API error updating records %d", status));
            return 0;
        }
    }

    printf("Updated %s.%s DNS zone: %s\n", host_name, domain, addr_str);

    return 1;
}


int main(int argc, char *argv[])
{
    int ret = 0;

    if (argc >= 2 && strcmp(argv[1], "-n") == 0)
        dry_run = 1;

    xs *config = open_config();

    if (config == NULL)
        ret = 1;
    else
    if (xs_type(config) == XSTYPE_LIST) {
        xs_list *p = config;
        xs_dict *v;

        while (xs_list_iter(&p, &v)) {
            if (!dialog(v))
                ret = 2;
        }
    }
    else
    if (!dialog(config))
        ret = 2;

    return ret;
}
